<?php
$nick = $_SESSION['nick'];
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>




    <style>

        #ChatWindow{

            background-color: #4cae4c;
            height: 400px;
        }

        #WhoIsOnline{
            height: 400px;
            background-color: burlywood;
        }

        #ChatroomTitle{
            background-color: white;
        }

    </style>
</head>
<body>


 <div class="container">


     <div id="ChatWindow" class="col-md-9">
         <div id="ChatroomTitle">

             <h3>Hello <span style="color: red"> <?= $nick ?></span>! Welcome to Code Station Chatroom</h3>

             <div style="text-align: center" id="NewMsgAlert"> </div>

         </div>

         <textarea id="ChatHistory" disabled rows="15" cols="96"> <?= file_get_contents("ChatHistory.txt")?> </textarea>
         <div id ="ChatMsg"> <input size="93" id="MSG" type="text"> </div>

     </div>

     <div id="WhoIsOnline" class="col-md-3">
        <textarea id="online" disabled rows="15" cols="20">

        </textarea>


     </div>




 </div>

 <script>


     $(document).ready(function () {

         $("#NewMsgAlert").click(function () {
             document.getElementById("ChatHistory").scrollTop =   document.getElementById("ChatHistory").scrollHeight ;
         });

         $("#MSG").keypress(function(e) {
             if (e.which == 13) {

                  var msg2send = document.getElementById("MSG").value;

                  $.post("../Chatroom/Write2File.php",
                      {
                          "WHO":"<?= $nick ?>",
                          "MSG": msg2send
                      },

                      function (data,status) {

                      }
                  );

                 document.getElementById("MSG").value = "";

             }
         });




     });


     var currentChatHistory;
     var preChatHistory = "";

     var myFlag1 = true;
     var scrollPercent ;


     setInterval(function () {
         var chatHistoryObj = document.getElementById('ChatHistory');

         $('#ChatHistory').scroll(function(){
              window.scrollPercent =100*this.scrollTop/this.scrollHeight/(1-this.clientHeight/this.scrollHeight);
            // $('#WhoIsOnline').html(window.scrollPercent.toFixed(2)+'%');
         });


        // var scrollPercent =  (chatHistoryObj.scrollHeight - chatHistoryObj.scrollTop)*100/chatHistoryObj.scrollHeight;

         if(scrollPercent >90){
             $("#NewMsgAlert").fadeOut(500);
             chatHistoryObj.scrollTop = chatHistoryObj.scrollHeight;
         }

         $(document).ready(function () {
             $.post("ChatHistory.txt", function (data, status) {
                   window.currentChatHistory = data;
             })
         });





         if (window.preChatHistory != window.currentChatHistory) {


             //alert(chatHistoryObj.scrollTop + " "+chatHistoryObj.scrollHeight);

               chatHistoryObj.innerHTML = window.currentChatHistory;

               if(chatHistoryObj.innerHTML == "undefined")
                   chatHistoryObj.innerHTML = "Initializing... ";




             //alert(scrollPercent);

             if(window.scrollPercent >90){
                 chatHistoryObj.scrollTop = chatHistoryObj.scrollHeight ;

             }
             else{

                    var newMsgObj = document.getElementById("NewMsgAlert");

                    newMsgObj.innerHTML = "New Message Arrived";

                    $("#NewMsgAlert").fadeOut(500);
                    $("#NewMsgAlert").fadeIn(500);



             }


             window.preChatHistory = window.currentChatHistory;


             if(window.myFlag1 && chatHistoryObj.scrollHeight >350 ) {
                 chatHistoryObj.scrollTop = chatHistoryObj.scrollHeight ;
                 window.myFlag1 = false;
             }

         }
         $(document).ready(function () {
             $.post("online.txt", function (data, status) {
                 var online=document.getElementById('online');
                 online.innerHTML = data;
             })
         });

     },500);
     var time;
     setInterval(function () {
         $.post("gettime.php", function (timedata, status) {
             window.time=timedata;
         });
     $.post("whoisonline.php",
         {

             "WHO": "<?= $nick ?>",
             "TIME": window.time,

         },
         function(data,status){
        // alert(data);
         });


     },1000);

 </script>

</body>
</html>