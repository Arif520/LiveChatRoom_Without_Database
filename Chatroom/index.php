<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js"></script>


    <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
<?php
session_start();

if(isset($_POST['nick'])){
    $_SESSION['nick'] = $_POST['nick'];
}

if(isset($_SESSION['nick'])){

    showChatWindow();
}
else{

    showLoginWindow();
}



function showLoginWindow(){

    include "LoginBox/index.html";

}

function showChatWindow(){
    include "ChatWindow/index.php";
}


?>
</body>
</html>


